import React, { Component } from 'react';
import './App.css';
import PlayerCards from './PlayerCards';

const apiUrl = 'http://localhost:8080';

const CONFIG = {
	maxHumans: 4,
	maxBots: 3,
	gameStates: {
		none: 1,
		dbInit: 2,
		giveBags: 3,
		gameInit: 4,
		startGame: 5,
		playing: 6
	}
};

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			gameStarted: false,
			cardsGiven: false,
			error: null,
			currentPlayerId: null,
			gameConfig: null,
			playerCards: null
		};

		// This binding is necessary to make `this` work in the callback
		this.startGameClick = this.startGameClick.bind(this);
		this.giveBagsClick = this.giveBagsClick.bind(this);
		this.goToNextPlayer = this.goToNextPlayer.bind(this);
	}

	gameLoaded() {}

	componentDidMount() {
		this.setState({
			loading: true
		});
		fetch(`${apiUrl}/config/load`).then((res) => res.json()).then(
			(result) => {
				this.setState({
					loading: false,
					gameConfig: result
				});
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				this.setState({
					loading: false,
					error: error
				});
			}
		);
	}

	errorLoadingGame() {
		this.setState({
			gameStarted: false,
			error: 'Error loading game',
			isLoading: false
		});
	}

	async getPlayerCards(playerNumber, isHuman = true) {
		return fetch(`${apiUrl}/game/getPlayerInformation/${isHuman === true ? 'b' : 'h'}/${playerNumber}`);
	}
	async giveBags() {
		let playerCards = [];
		for (let i = 0; i < this.state.gameConfig.humanPlayersNumber; i++) {
			let cards = await this.getPlayerCards(i + 1).then((res) => res.json());
			console.log(cards);
			playerCards.push(cards);
		}

		for (let i = 0; i < this.state.gameConfig.botPlayersNumber; i++) {
			let cards = await this.getPlayerCards(i + 1, false).then((res) => res.json());
			console.log(cards);
			playerCards.push(cards);
		}

		this.setState({ playerCards, currentPlayerId: 0 });
	}

	giveBagsClick(event) {
		this.giveBags();
	}

	startGameClick(event) {
		this.setState({
			isLoading: true,
			error: null
		});
		fetch(`${apiUrl}/game/db/init`).then((res) => res.json()).then(
			(result) => {
				if (result === true) {
					this.setState({
						gameStarted: true,
						isLoading: false
					});
				} else {
					this.errorLoadingGame();
				}
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				console.log(error);
				this.errorLoadingGame();
			}
		);
	}
	goToNextPlayer() {
		this.setState((state) => ({ currentPlayerId: state.currentPlayerId + 1 }));
	}
	getPlayerConfig(playerIndex, isHuman = true) {
		return {
			name: this.state.gameConfig[`${isHuman === true ? 'human' : 'bot'}Player${playerIndex + 1}Name`],
			color: this.state.gameConfig[`${isHuman === true ? 'human' : 'bot'}Player${playerIndex + 1}Color`]
		};
	}

	getHumanConfig(humanIndex) {
		return this.getPlayerConfig(humanIndex);
	}

	getBotConfig(botIndex) {
		return this.getPlayerConfig(botIndex, false);
	}

	findPlayerCardsByName(playerName) {
		return this.state.playerCards.find(playerName);
	}

	async gameInit() {
		let botsCards = [];
		for (let i = 0; i < this.state.gameConfig.botPlayersNumber; i++) {
			let botConfig = this.getBotConfig(i);
			let botCards = this.findPlayerCardsByName(botConfig.name);
			botsCards.push(botCards);
		}

		let botsInitData = await fetch(`${apiUrl}/game/init`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(botsCards)
		}).then((res) => res.json());

		console.log(botsInitData);
	}

	render() {
		return (
			<div className="App">
				<h2>Game Started ? {this.state.gameStarted ? 'Yeah!' : 'Nein'}</h2>
				{this.state.gameStarted === false && <button onClick={this.startGameClick}> Start </button>}
				{this.state.gameStarted === true &&
				this.state.playerCards === null && <button onClick={this.giveBagsClick}> Give Cards </button>}
				{this.state.showPlayerCards === true &&
				this.state.currentPlayerId !== null && (
					<PlayerCards
						player={this.state.playerCards[this.state.currentPlayerId]}
						goToNextPlayer={this.goToNextPlayer}
					/>
				)}
				{this.state.playerCards !== null &&
				this.state.currentPlayerId === this.state.playerCards.length && (
					<button onClick={this.giveBagsClick}> Give Cards </button>
				)}
				{this.state.isLoading && <p>Loading...</p>}
				{this.state.error !== null && <p>Error: {this.state.error}</p>}
				{this.state.gameConfig !== null && (
					<div id="configData">
						<h3>Game Configuration</h3>
						<p>
							<b>Humans: </b>
							{this.state.gameConfig.humanPlayersNumber}
							- <b>Bots: </b>
							{this.state.gameConfig.botPlayersNumber}
						</p>
					</div>
				)}
			</div>
		);
	}
}

export default App;
