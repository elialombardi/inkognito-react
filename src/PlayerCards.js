import React, { Component } from 'react';

class PlayerCards extends Component {
	render() {
		return (
			<div className="playerCards">
				<p>
					<b>{this.props.player.name}</b> - {this.props.player.color}
				</p>
				<p>
					{this.props.player.playerCards.costume}, {this.props.player.playerCards.identity},
					{this.props.player.playerCards.mission}
				</p>
				<button onClick={this.props.goToNextPlayer}>Next Player</button>
			</div>
		);
	}
}

export default PlayerCards;
